import os
import subprocess
def walk(dirname):
    '''
    递归查找给定后缀的所有路径
    :return:
    '''
    name=[]
    if '__pycache__' in name:
        return name
    for na in os.listdir(dirname):
        path=os.path.join(dirname,na)
        if os.path.isfile(path):
            name.append(path)
        if os.path.isdir(path):
            name.extend(walk(path))
    return name
def compute_checksum(filename):
    '''
    计算文件中的内容的md5sum值
    :param filename:
    :return:
    '''
    print(filename)
    cmd='md5sum '+filename
    return pipe(cmd)
def check_diff(name1,name2):
    '''
    计算两个文件之间的差异
    :param name1:
    :param name2:
    :return:
    '''
    cmd='diff %s %s'%(name1,name2)
    return pipe(cmd)
def pipe(cmd):
    '''
    运行命令
    :param cmd:
    :return:
    '''
    fp=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    res=fp.stdout.read()
    stat=fp.poll()
    print(stat)
    assert stat is None#stat 不为None时崩溃
    return res,stat
def compute_checksums(dirname,suffix):
    '''
    计算给定后缀的所有文件的md5sum
    :param names:
    :return:
    '''
    d={}
    path=walk(dirname)
    for name in path:
        if name.endswith(suffix):
            res,stat=compute_checksum(name)
            cod, _ =res.split()
            if cod in d:
                d[cod].append(name)
            else:
                d[cod]=[name]
    return d

def check_pairs(names):
    '''
    检查是否存在不同的文件
    :param names:
    :return:
    '''
    for name1 in names:
        for name2 in names:
            if name1<name2:
                res,stat=check_diff(name1,name2)
                if res:
                    return False
    return True
def print_duplicated(d):
    '''
    打印出具有相同的md5sum的文件名和具有唯一的md5sum值的文件名
    :param d:
    :return:
    '''
    for key,value in d.items():
        if len(value)>1:
            print('the following path has the same checksum:')
            for name in value:
                print(name)
            if check_pairs(value):
                print('they are identical')
if __name__=='__main__':
    d=compute_checksums('.','.py')#计算当前路径下，后缀名为.py的所有文件
    print_duplicated(d)