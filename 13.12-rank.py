import matplotlib.pyplot as plt
import string
import sys
def process_file(filename):
    '''
     传入文件
    :param filename:
    :return:
    '''
    hist={}
    fp=open(filename,encoding='UTF-8')
    skip_header(fp)
    for line in fp:
        if line.startswith('*** END OF THIS'):
            break
        process_line(line,hist)
    return word_rank(hist)
def skip_header(fp):
    for line in fp:
        if line.startswith('*** START OF THIS'):
            break
def process_line(line,hist):
    '''
    传入line和字典hist，分割line为word，去掉符号
    :param line:
    :param hist:
    :return:
    '''
    line=line.replace('_',' ')
    punc=string.whitespace+string.punctuation
    for word in line.split():
        word.strip(punc)
        word.lower()
        hist[word]=hist.get(word,0)+1
def word_rank(hist):
    '''
    传入字典hist,进行排序
    :param hist:
    :return:
    '''
    res=[]
    for key, values in hist.items():
        res.append((values,key))
    # res.sort(reverse=True)
    # rank=[]
    # for i in range(1,len(res)+1):
    #     rank.append((res[i-1][0],i))
    fre=list(hist.values())
    fre.sort(reverse=True)
    rank=[(r+1,num) for r,num in enumerate(fre)]
    return rank
def plot_fre(rank,scale='log'):
    '''
    绘图logf=logc-slogr
    :param rank:
    :return:
    '''
    rs,rf=zip(*rank)
    plt.clf()
    plt.xscale(scale)
    plt.yscale(scale)
    plt.title('the frequence verse rank')
    plt.xlabel('rank')
    plt.ylabel('frequence')
    plt.plot(rs,rf,'r-',linewidth=3)
    plt.show()
def main(script,filename='158-0.txt',flag='plot'):
    '''
    main函数
    :return:
    '''
    rank=process_file(filename)
    if flag=='print':
        for num,fre in rank:
            print(num,fre)
    if flag=='plot':
        plot_fre(rank)

if __name__=='__main__':
    main(*sys.argv)



